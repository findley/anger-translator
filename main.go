package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	openai "github.com/sashabaranov/go-openai"
)

type AngryResp struct {
	Transcription string `json:"transcription"`
	Angry         string `json:"angry"`
}

func main() {
	http.HandleFunc("/api/aggressive", handleUpload)
	http.HandleFunc("/api/hello", handleHello)

	fs := http.FileServer(http.Dir("./web"))
	http.Handle("/", fs)

	fmt.Println("Starting server")
	http.ListenAndServe("0.0.0.0:8080", nil)
}

func handleUpload(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Got a file upload! Content type is: %s\n", r.Header.Get("Content-Type"))

	oaClient := openai.NewClient(os.Getenv("OPEN_AI_API_KEY"))
	transcribeReq := openai.AudioRequest{
		Model:    openai.Whisper1,
		FilePath: "input.ogg",
		Reader:   r.Body,
	}
	transcribeResp, err := oaClient.CreateTranscription(context.Background(), transcribeReq)
	if err != nil {
		w.WriteHeader(500)
		fmt.Printf("Transcription error: %v\n", err)
		return
	}

	fmt.Printf("Transribed text: %s\n", transcribeResp.Text)

	userMsg := fmt.Sprintf(`Message to translate aggressively:
`+"```"+`
%s
`+"```", transcribeResp.Text)

	fmt.Printf("User message: %s\n", userMsg)

	aggressiveReq := openai.ChatCompletionRequest{
		Model:     openai.GPT3Dot5Turbo,
		MaxTokens: 100,
		Messages: []openai.ChatCompletionMessage{
			{
				Role:    openai.ChatMessageRoleSystem,
				Content: "You are a bot that translates the users message into a more angry or aggressive phrasing. This is based on the SNL 'Anger Translator' skit where president Obama has an anger translator who rephrases his speech to be more angry.",
			},
			{
				Role:    openai.ChatMessageRoleUser,
				Content: userMsg,
			},
		},
	}

	aggressiveResp, err := oaClient.CreateChatCompletion(context.Background(), aggressiveReq)
	if err != nil {
		w.WriteHeader(500)
		fmt.Printf("Aggressive completion error: %v\n", err)
		return
	}
	aggressiveTranslation := aggressiveResp.Choices[0].Message.Content

	fmt.Printf("User message: %s\n", aggressiveTranslation)

	resp := AngryResp{
		Transcription: transcribeResp.Text,
		Angry:         aggressiveTranslation,
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

func handleHello(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hi Emily")
	fmt.Fprintf(w, "Hi Emily")
}
