# Anger Translator 😠

Inspired by the [SNL Anger Translator Skit](https://www.youtube.com/watch?v=-qv7k2_lc0M)
this app lets the user record an audio message, transcribes it, then adjusts it
to by appropriately angry. It uses OpenAI's wisper and chatGPT APIs to make the
magic happen.
