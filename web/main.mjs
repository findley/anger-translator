const logDiv = document.getElementById("log");

const recordingButton = document.getElementById("record-button");

function makeButtonActive() {
    recordingButton.classList.add('active');
    recordingButton.classList.remove('loading');
    recordingButton.disabled = false;
}

function makeButtonLoading() {
    recordingButton.classList.remove('active');
    recordingButton.classList.add('loading');
    recordingButton.disabled = true;
}

function makeButtonDefault() {
    recordingButton.classList.remove('active');
    recordingButton.classList.remove('loading');
    recordingButton.disabled = false;
}

let recordingTimeout = null;

function findMatchingSupportedMediaType() {
    if (MediaRecorder.isTypeSupported('audio/ogg; codecs=vorbis')) {
        return 'audio/ogg; codecs=vorbis'
    }

    if (MediaRecorder.isTypeSupported('audio/mp3')) {
        return 'audio/mp3'
    }

    return null;
}

navigator.mediaDevices.getUserMedia({ audio: true }).then(
    (stream) => {
        const type = findMatchingSupportedMediaType();
        let options = undefined;
        if (type) {
            options = { mimeType: type};
        }
        const mediaRecorder = new MediaRecorder(stream, options);
        console.log('created media recorder');

        function startRecording() {
            console.log('starting recording');
            recordingTimeout = setTimeout(() => stopRecording(), 30000);
            mediaRecorder.start();
            makeButtonActive();
        }

        function stopRecording() {
            clearTimeout(recordingTimeout);
            recordingTimeout = null;
            console.log('stopping recording');
            mediaRecorder.stop();
            makeButtonLoading();
        }

        recordingButton.addEventListener('click', () => {
            const recording = recordingButton.classList.contains('active');
            if (recording) {
                stopRecording();
            } else {
                startRecording();
            }
        });

        let audioChunks = [];
        mediaRecorder.ondataavailable = function(e) {
            console.log('got audio data chunk');
            audioChunks.push(e.data);
        }

        mediaRecorder.onstop = function(e) {
            console.log("data available after MediaRecorder.stop() called.");

            const blob = new Blob(audioChunks, { 'type': mediaRecorder.mimeType });
            audioChunks = [];

            fetch('/api/aggressive', { method: 'POST', body: blob })
                .then(response => response.json())
                .then(resp => {
                    const transcriptionP = document.createElement('p');
                    const transcriptionL = document.createElement('b');
                    const transcriptionS = document.createElement('span');
                    transcriptionL.innerText = "Transcription: ";
                    transcriptionS.innerText = resp.transcription;
                    transcriptionP.append(transcriptionL, transcriptionS);

                    const angryP = document.createElement('p');
                    const angryL = document.createElement('b');
                    const angryS = document.createElement('span');
                    angryL.innerText = "Angry💢: ";
                    angryL.classList.add("angry");
                    angryS.innerText = resp.angry;
                    angryP.append(angryL, angryS);

                    logDiv.append(transcriptionP, angryP);
                    makeButtonDefault();
                });
        }
    },
    (err) => console.log(err)
);
